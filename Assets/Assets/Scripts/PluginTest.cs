using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class PluginTest : MonoBehaviour {

	public const string pluginName = "phonebook.example.com.addcontact.Contact";
	static AndroidJavaClass _pluginClass;
	static AndroidJavaObject _pluginInstance;

	public CallBackHandler callback;

	#if UNITY_IOS && !UNITY_EDITOR
	[DllImport("__Internal")]
	private static extern void _ex_callSwiftMethod(string firstName,string lastName,string company,string number);
	#endif

	public Text text;

	// Use this for initialization
	void Start () {
//		text.text = printHello ();
		//addDummyContact ();
	}
	/*
	public static AndroidJavaClass PluginClass
	{
		get{ 
			if (_pluginClass == null) {
				_pluginClass = new AndroidJavaClass (pluginName);
			}
			return _pluginClass;
		}
	}

	public static AndroidJavaObject PluginInstance
	{
		get{ 
			if (_pluginInstance == null) {
			
				_pluginInstance = new PluginClass.CallStatic<AndroidJavaObject> ("getInstance");
			}
			return _pluginInstance;
		}
	}*/
	
	// Update is called once per frame
	void Update () {
		
	}

	public void addContact(string firstName, string lastName, string company, string number)
	{
		if (Application.platform == RuntimePlatform.Android) {
			//return PluginInstance.Call<string> ("helloWorld",7);
			var plugin = new AndroidJavaClass (pluginName);
			AndroidJavaClass unityPlayer = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject> ("currentActivity");
			AndroidJavaObject context = activity.Call<AndroidJavaObject> ("getApplicationContext");
			object[] par = new object[4];
			par [0] = firstName+" "+lastName;
			par [1] = number;
			par [2] = context;
			par [3] = company;
			string success = plugin.CallStatic<string> ("WritePhoneContact", par);
			if (success.Equals ("done")) {
				//GameObject.FindGameObjectWithTag ("callback").GetComponent<CallBackHandler> ().OnCallFromSwift ("Success");
				callback.OnCallFromSwift("done");
			}
		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
			CallSwiftMethod (firstName,lastName,company,number);
		}
	}

	// Use this method to call Example.swiftMethod() in Example.swift
	// from other C# classes.
	public static void CallSwiftMethod(string firstName,string lastName,string company,string number) {
		#if UNITY_IOS && !UNITY_EDITOR
		_ex_callSwiftMethod(firstName,lastName,company,number);
		#endif
	}

//	string printHello()
//	{
//		if (Application.platform == RuntimePlatform.Android) {
//			//return PluginInstance.Call<string> ("helloWorld",7);
//			var plugin = new AndroidJavaClass (pluginName);
//			return plugin.CallStatic<string> ("helloWorld", 7);
//		}
//		if (Application.platform == RuntimePlatform.IPhonePlayer) {
//			return helloWorld ().ToString();
//		}
//		return "Not Supported";
//	}
}
